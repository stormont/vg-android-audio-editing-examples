package com.example.examplemusiceditor;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.view.Menu;

import com.voyagegames.core.audio.editing.effects.AttenuationEffect;
import com.voyagegames.core.audio.editing.interfaces.IEffect;
import com.voyagegames.core.audio.editing.interfaces.IWavSample;
import com.voyagegames.core.audio.editing.modules.Frequency;
import com.voyagegames.core.audio.editing.modules.WavFile;
import com.voyagegames.core.audio.editing.modules.WavSample16Bit;

public class MainActivity extends Activity {
	
	private final int mSampleRate = 8000; //44100;
	
    private List<IWavSample> mScale = new ArrayList<IWavSample>();
	private IWavSample mSample;
	private AudioTrack mAudioTrack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final int scaleMillis = 250;

		mScale.add(new WavSample16Bit(mSampleRate, scaleMillis, Frequency.C_4, WavFile.UNCOMPRESSED_PCM_FORMAT));
		mScale.add(new WavSample16Bit(mSampleRate, scaleMillis, Frequency.D_4, WavFile.UNCOMPRESSED_PCM_FORMAT));
		mScale.add(new WavSample16Bit(mSampleRate, scaleMillis, Frequency.E_4, WavFile.UNCOMPRESSED_PCM_FORMAT));
		mScale.add(new WavSample16Bit(mSampleRate, scaleMillis, Frequency.F_4, WavFile.UNCOMPRESSED_PCM_FORMAT));
		mScale.add(new WavSample16Bit(mSampleRate, scaleMillis, Frequency.G_4, WavFile.UNCOMPRESSED_PCM_FORMAT));
		mScale.add(new WavSample16Bit(mSampleRate, scaleMillis, Frequency.A_4, WavFile.UNCOMPRESSED_PCM_FORMAT));
		mScale.add(new WavSample16Bit(mSampleRate, scaleMillis, Frequency.B_4, WavFile.UNCOMPRESSED_PCM_FORMAT));
		mScale.add(new WavSample16Bit(mSampleRate, scaleMillis, Frequency.C_5, WavFile.UNCOMPRESSED_PCM_FORMAT));
		
		mSample = new WavSample16Bit(mSampleRate, 3000, Frequency._MIDDLE_C, WavFile.UNCOMPRESSED_PCM_FORMAT);
    }

    @Override
    protected void onResume() {
        super.onResume();
		
		// This can take a while; spin it off in a thread
		new Thread(new Runnable() {
            public void run() {
            	final IEffect dropOff = new AttenuationEffect(1.0, 0.0);
            	
            	for (final IWavSample s : mScale) {
            		s.generateSample();
            		dropOff.apply(s);
            		s.finalizeOutput();
            	}
            	
            	mSample.generateSample();
            	new AttenuationEffect(1.0, 0.0).apply(mSample);
            	mSample.finalizeOutput();
            	
    			playSound(mSample.generated());
    			//playScale();
            }
		}).start();
	}
    
    @Override
    protected void onPause() {
    	super.onPause();
    	
    	if (mAudioTrack != null) {
    		mAudioTrack.stop();
    	}
    }
    
    @Override
    protected void onDestroy() {
    	if (mAudioTrack != null) {
    		mAudioTrack.stop();
    	}
    	
    	super.onDestroy();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

    void playSound(final byte[] sample) {
        if (mAudioTrack != null) {
        	mAudioTrack.stop();
        }
        
        mAudioTrack = new AudioTrack(
        		AudioManager.STREAM_MUSIC,
        		mSampleRate,
        		AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                sample.length,
                AudioTrack.MODE_STATIC);
        mAudioTrack.write(sample, 0, sample.length);
        mAudioTrack.play();
    }

    void playScale() {
    	final List<byte[]> generated = new ArrayList<byte[]>();
    	
    	for (final IWavSample s : mScale) {
    		generated.add(s.generated());
    	}

        try {
        	for (int i = 0; i < generated.size(); i++) {
            	playSound(generated.get(i));
    			Thread.sleep(mScale.get(i).durationInMilliSeconds());
        	}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
